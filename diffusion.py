from firedrake import *
import numpy as np
from matplotlib import pyplot as plt

'''
Firedrake for solving coupled equation system on the interval [0,L_1+L_2]
=========================================================================

  We seek the solution u(x) on [0,L] with L = L_1 + L_2

          { u_1(x) for x in [0,L_1]
  u(x) =  {
          { u_2(x) for x in [L_1,L]

  where:

    u_1(x=0) = a_{-}                                  for x = 0
    D_{diff,1}*u_1''(x) = f_{-}                       for x in (0,L_1)
    -D_{diff,1}*u_1'(x=L_1) = -D_{diff,2}*u_2'(x=L_1) for x = L_1
    K_1*u_1(x=L_1) = K_2*u_2'(x=L_1)                  for x = L_1
    D_{diff,2}*u_2''(x) = f_{+}                       for x in (L_1,L)
    u_2(x=L) = a_{+}                                  for x = 0

  The constants contained in this are:
  * diffusion coefficients D_{diff,1} and D_{diff,2}
  * continuity coefficients K_1 and K_2
  * boundary values a_{-} and a_{+}

  For constant f_{-}, f_{+} we can derive the exact solution, which is given
  by

    u_1^{exact}(x) = a_{-} + alpha_1*x + f_{-}/(2*D_{diff_1})*x^2
    u_2^{exact}(x) = a_{+} + alpha_2*(x-L) + f_{+}/(2*D_{diff_2})*(x-L)^2

  where the coefficients alpha_1 and alpha_2 can be obtained by solving a 
  2 x 2 linear system arising from the interface conditions.

  The problem is solved in a P1 finite element space. Let the interface
  node be the node at x=L_1.
  To solve the equation in Firedrake, we construct a mixed function space,
  consisting of three P1 spaces on [0,L]. The spaces have the following
  Dirichlet 'boundary' nodes for constraining the dofs:
  
  * space 1 (for function u_1): all nodes right of the interface node are
    constrained, plus the true Dirichlet BC at x=0
  * space 2 (for function u_2): all nodes left of the interface node are
    constrained, plus the true Dirichlet BC at x=L
  * space 3 (for the Lagrange multiplier): all nodes except the interface
    node are constrained
'''

# ==== Parameters ====
# Number of cells in x-direction
nx = 16

# Size of domain
L1 = 0.8
L2 = 1.2

# Diffusion constants D_{diff,i}
D_diff_1 = 0.4
D_diff_2 = 0.9

# Continuity constants K_i
K_1 = 1.0
K_2 = 1.5

# Value at Dirichlet boundaries
a_minus = 1.0
a_plus = 2.0

# Forcing functions (RHS)
f_minus = 0.7
f_plus = 0.9

def show_matrix(a_weak,bcs,sparsity_only=False):
    '''Visualise system matrix of 3x3 weak form

    :arg a_weak: The weak form in the mixed space
    :arg bcs: boundary conditions
    :arg sparsity_only: Show sparsity pattern only? 
    '''
    for i_block in range(3):
        M = []
        for j_block in range(3):
            M_tmp = assemble(a_weak,bcs=bcs).M.handle.getNestSubMatrix(i_block,j_block)
            nrows,ncols = M_tmp.getSize()
            M.append(np.asarray(M_tmp.getValues(range(nrows),range(ncols))))
        nrows = M[0].shape[0]
        for i in range(nrows):
            s = ''
            for j_block in range(3):
                M_tmp = M[j_block]
                nclos = M_tmp.shape[1]
                if (sparsity_only):
                    for j in range(ncols):
                        if (abs(M_tmp[i,j])>1.E-16):
                            s += '*'
                        else:
                            s += ' '
                    s += '|'
                else:
                    for j in range(ncols):
                        s += ('%4.1f ' % M_tmp[i,j])
                    s += '| '
            print (s)
        print ('')

def solution_exact():
    '''Calculate exact solution and return list of two function objects
    '''
    A = np.asarray(((-K_1*L1, -K_2*L2),
                    (-D_diff_1,+D_diff_2)))
    b = np.asarray((K_1*a_minus-K_2*a_plus+0.5*(L1**2*K_1/D_diff_1*f_minus-L2**2*K_2/D_diff_2*f_plus),
                    (f_minus*L1+f_plus*L2)))
    alpha = np.linalg.solve(A,b)
    func_1 = lambda x: a_minus + alpha[0]*x+0.5*f_minus/D_diff_1*x**2
    func_2 = lambda x: a_plus + alpha[1]*(x-(L1+L2))+0.5*f_plus/D_diff_2*(x-(L1+L2))**2
    return (np.vectorize(func_1), np.vectorize(func_2))

# Construct mesh for interval [0,L]
mesh = IntervalMesh(nx,L1+L2)

# Function spaces
V_W = FunctionSpace(mesh, "CG", 1)
V_DG = FunctionSpace(mesh, "DG", 0)
# Extract mesh coordinates, which are needed to work out where
# the interface at x=L_1 is 
X = mesh.coordinates
x_coord = Function(V_DG).interpolate(X[0])

# Construct indicator functions in piecewise constant DG space
# Ind_1(x) is 1 for x in [0,L_1] and zero otherwise
# Ind_2(x) is 1 for x in [L_1,L] and zero otherwise
Ind_1 = Function( V_DG)
par_loop( 'f[0][0] = (g[0][0]<L1[0]);',dx,
          {'f': (Ind_1, WRITE),
           'g':(x_coord, READ),
           'L1':(Constant(L1),READ)} )
Ind_2 = Function( V_DG)
par_loop( 'f[0][0] = (g[0][0]>L1[0]);',dx,
          {'f': (Ind_2, WRITE),
           'g':(x_coord, READ),
           'L1':(Constant(L1),READ)} )

# Construct indicator functions in P1 space. Those are needed to
# work out the constrained nodes in the solution- and Lagrange-multiplier
# spaces: nodes for which I_cg_x is 0 are treated as boundary nodes by
# the class ConstrainedBC below
# I_cg_1 is 1 for all nodes in the domain [0,L_1) and zero otherwise
# I_cg_2 is 1 for all nodes in the domain (L_1,L] and zero otherwise
# I_cg_int is 1 only for the node on the interface
I_cg_1 = Function(V_W)
I_cg_1.assign(0.0)
par_loop( 'for (int i=0; i<A.dofs; ++i) A[i][0] = fmax(A[i][0], B[0][0]);',
          dx, {'A' : (I_cg_1, RW), 'B': (Ind_1,READ)})
I_cg_2 = Function(V_W)
I_cg_2.assign(0.0)
par_loop( 'for (int i=0; i<A.dofs; ++i) A[i][0] = fmax(A[i][0], B[0][0]);',
          dx, {'A' : (I_cg_2, RW), 'B': (Ind_2,READ)})
I_cg_int = Function(V_W)
I_cg_int.assign(0.0)
par_loop( 'for (int i=0; i<A.dofs; ++i) A[i][0] = fmin(B[i][0],C[i][0]);',
          dx, {'A' : (I_cg_int, WRITE),
               'B': (I_cg_1,READ),
               'C': (I_cg_2,READ)})

class ConstrainedBC(DirichletBC):
    '''Constrained 'boundary' condition class. This constrains all
    nodes for which marker_i = 0, by treating them as Dirichlet nodes.

    :arg V: function space
    :arg value: value the function at the constrained nodes will be set to
    :arg makers: function with marker values
    '''
    def __init__(self, V, value, markers, sub_domain=0, method='topological'):
        super(ConstrainedBC, self).__init__(V, value, sub_domain, method=method)
        self.markers = markers
        self.value = value
        self.nodes = np.unique(np.where(markers.dat.data_ro_with_halos==0)[0])

    def reconstruct(self, *, V=None, g=None, sub_domain=None, method=None):
        if V is None:
            V = self.function_space()
        if g is None:
            g = self._original_arg
        if sub_domain is None:
            sub_domain = self.sub_domain
        if method is None:
            method = self.method
        if V == self.function_space() and g == self._original_arg and \
           sub_domain == self.sub_domain and method == self.method:
            return self
        return type(self)(V, g, self.markers, sub_domain, method=method)
    
# Function space for solution and Lagrange multiplier
V_u = FunctionSpace(mesh, "CG", 1)
V_lagrange = FunctionSpace(mesh, "CG", 1)

# Construct mixed function space for solution
V_mixed = V_u * V_u * V_lagrange

# Boundary ids of interval mesh (see Firedrake documentation)
bottom_id = 1
top_id = 2

# True Dirichlet boundary conditions at x=0 and x=L 
BC_bottom = DirichletBC(V_mixed.sub(0), a_minus, bottom_id)
BC_top = DirichletBC(V_mixed.sub(1), a_plus, top_id)
# Constrain all nodes with ConstrainedBC 
BC_exclude_outside_1 = ConstrainedBC(V_mixed.sub(0), 0.0, I_cg_1)
BC_exclude_outside_2 = ConstrainedBC(V_mixed.sub(1), 0.0, I_cg_2)
BC_exclude_outside_int = ConstrainedBC(V_mixed.sub(2), 0.0, I_cg_int)

# Construct normal vector which is only non-zero on the interface
n_vec = FacetNormal(mesh)
n_int = Ind_1("+")*n_vec("+") + Ind_1("-")*n_vec("-")

# Trial- and test- functions of mixed function space 
u_trial_1, u_trial_2, u_trial_int = TrialFunctions(V_mixed)
v_test_1, v_test_2, v_test_int = TestFunctions(V_mixed)

# Construct weak form
# Part Ia: D_{diff_1}*u_1''(x) for x in (0,L_1)
a_weak = -Ind_1*D_diff_1*inner(grad(v_test_1),grad(u_trial_1))*dx
# Part Ib: D_{diff_2}*u_2''(x) for x in (L_1,L)
a_weak += -Ind_2*D_diff_2*inner(grad(v_test_2),grad(u_trial_2))*dx
# Part II: Dirichlet interface condition
#          -D_{diff,1}*u_1'(x=L_1) = -D_{diff,2}*u_2'(x=L_1)
a_weak += (-D_diff_1*Ind_1('+')*v_test_2('+')*inner(grad(u_trial_1('+')),n_int)
           -D_diff_1*Ind_1('-')*v_test_2('-')*inner(grad(u_trial_1('-')),n_int)
           +D_diff_2*Ind_2('+')*v_test_1('+')*inner(grad(u_trial_2('+')),n_int)
           +D_diff_2*Ind_2('-')*v_test_1('-')*inner(grad(u_trial_2('-')),n_int))*dS

# Part III: von Neumann interface condition K_1*u_1(x=L_1) = K_2*u_2'(x=L_1) 
a_weak += avg(v_test_int)*(K_1*avg(u_trial_1)-K_2*avg(u_trial_2))*dS
a_weak += avg(u_trial_int)*(K_1*avg(v_test_1)-K_2*avg(v_test_2))*dS

# Collect boundary conditions
bcs = [BC_bottom,
       BC_top,
       BC_exclude_outside_1,
       BC_exclude_outside_2,
       BC_exclude_outside_int]

# Visualise matrix
show_matrix(a_weak, bcs, True)

# Solution in mixed space
u = Function(V_mixed)

# Construct right hand side
# f(x) = f_{-} for x in (0,L_1) and f_{+} for x in (L_1,L)
L = (f_minus*Ind_1*v_test_1+f_plus*Ind_2*v_test_2)*dx

# Build linear variational problem
lvp = LinearVariationalProblem(a_weak,L, u, bcs=bcs)

# Solve problem with given solver parameters
# Relative tolerance in solver
rtol = 1.E-12
# Use improved Schur-complement solver?
use_schur_solver=False
if (use_schur_solver):
    solver_param = {'ksp_rtol':rtol,
                    'ksp_type':'gmres',
                    'mat_type':'aij',
                    'pc_type':'fieldsplit',
                    'pc_fieldsplit_type': 'schur',
                    'pc_fieldsplit_schur_fact_type': 'full',
                    'pc_fieldsplit_0_fields':'0,1',
                    'pc_fieldsplit_1_fields':'2',
                    'pc_fieldsplit_0_ksp_type': 'gmres',
                    'pc_fieldsplit_0_pc_type': None,
                    'pc_fieldsplit_0_ksp_rtol': rtol,
                    'pc_fieldsplit_1_ksp_type': 'gmres',
                    'pc_fieldsplit_1_pc_type': None,
                    'pc_fieldsplit_1_ksp_rtol': rtol,
                    'ksp_monitor_true_residual':True}
else:
    solver_param = {'ksp_rtol':rtol,
                    'mat_type':'aij',
                    'ksp_type':'gmres',
                    'pc_type':'jacobi',
                    'ksp_monitor_true_residual':True}
lvs = LinearVariationalSolver(lvp,solver_parameters=solver_param)
lvs.solve()

def plot_solution(u):
    '''Visualise the solution by plotting it to a pdf file

    :arg u: Solution in mixed space
    '''
    u1, u2, uint = u.split()
    u1_data = u1.vector().dat.data
    u2_data = u2.vector().dat.data
    I_cg_1_data = I_cg_1.vector().dat.data
    I_cg_2_data = I_cg_2.vector().dat.data
    n = len(u1_data)
    X = (L1+L2)/(n-1.0)*np.asarray(range(n))
    plt.clf()
    X_1 = X[I_cg_1_data>0]
    X_2 = X[I_cg_2_data>0] 
    plt.plot(X_1,u1_data[I_cg_1_data>0],linewidth=2,color='blue',
             label=r'$u_1(x)$')
    plt.plot(X_2,u2_data[I_cg_2_data>0],linewidth=2,color='red',
             label=r'$u_2(x)$')
    plt.plot(X_1,solution_exact()[0](X_1),
             linewidth=2,
             color='green',
             linestyle='--',label='exact solution')
    plt.plot(X_2,solution_exact()[1](X_2),
             linewidth=2,
             color='green',
             linestyle='--')
    ax = plt.gca()
    ax.set_xlabel('x')
    ax.set_ylabel('$u(x)$')
    plt.legend(loc='upper left')
    plt.savefig('solution.pdf',bbox_inches='tight')

# Plot the solution
plot_solution(u)
